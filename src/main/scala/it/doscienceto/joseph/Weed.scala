package it.doscienceto.joseph

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

class Weed(drawable1:Drawable, drawable2:Drawable, height: Int, xPer:Float, yPer:Float, initialChoice: Boolean, zInd:Int) extends Entity {
  var choice = initialChoice;
  override def zIndex = zInd;
  override def draw(canvas: Canvas){
    val drawable = if(choice){
      drawable1
    } else {
      drawable2
    }
    val ih = drawable.getIntrinsicHeight();
    val iw = drawable.getIntrinsicWidth();
    val width = height * iw / ih;
    val cw = canvas.getWidth()
    val ch = canvas.getHeight()
    val x = (xPer * cw).toInt;
    val y = (yPer * ch).toInt;

    drawable.setBounds(x-width/2, y-height, x+width/2, y);
    drawable.draw(canvas);
  }
  def toggle(){
    choice = !choice;
  }

}
