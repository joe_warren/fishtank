package it.doscienceto.joseph

import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Matrix;

class Fish(srcBitmap: Bitmap, width: Int, yPercent: Float, direction: Int, speed: Float, zInd: Int) extends Entity {

  override def zIndex:Int = zInd;

  val scale = width.toFloat/srcBitmap.getWidth().toFloat;
  val matrix = new Matrix();
  matrix.preScale(scale*direction, scale);
  var percent = if(direction < 0){
    1.0f
  } else {
    0.0f
  }

  def ypercent: Float = yPercent;
  def xpercent: Float = percent;

  val bitmap = Bitmap.createBitmap(srcBitmap,
      0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, false);

  def height() = bitmap.getHeight()

  override def draw(canvas: Canvas){
      val cw = canvas.getWidth();
      val ch = canvas.getHeight();
      val offset = -width * (direction + 1)/2
      canvas.drawBitmap(bitmap, cw*percent + offset, yPercent*ch, null);
  }
  def move(){
    percent += direction * speed
  }
  def onScreen(screenWidth: Int):Boolean ={
    if(direction < 0){
      percent*screenWidth > -width 
    } else {
      percent*screenWidth < width + screenWidth
    }
  }
}
