package it.doscienceto.joseph
import android.graphics.Canvas

abstract class Entity {
  def draw(canvas:Canvas): Unit;
  def zIndex() : Int;
}
