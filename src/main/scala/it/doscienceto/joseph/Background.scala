package it.doscienceto.joseph

import android.graphics.Canvas;
import android.graphics.drawable.Drawable

class Background(drawable: Drawable){

    val w = drawable.getIntrinsicWidth()
    val h = drawable.getIntrinsicHeight()

    def draw(canvas: Canvas){
        val cw = canvas.getWidth()
        val ch = canvas.getHeight()
        val ratioW = cw.toFloat/w.toFloat
        val ratioH = ch.toFloat/h.toFloat
        val ratio = Math.max(ratioW, ratioH)
        // Always display the bottom part of the image, 
        // because that's where the sand is
        val yOffset = ch-(ratio*h) 
        drawable.setBounds(0, yOffset.toInt, (ratio*w).toInt, (yOffset+ratio*h).toInt);
        drawable.draw(canvas);
    }
}
