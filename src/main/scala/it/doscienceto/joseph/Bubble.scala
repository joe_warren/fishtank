package it.doscienceto.joseph

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

class Bubble(drawable: Drawable, burstDrawable: Drawable, xPercent: Float, yPercent: Float, speed: Float, size: Int, zInd: Int) extends Entity {
  override def zIndex = zInd;
  var yPer = yPercent;
  var sz = 0;
  var isBurst = false;
  var burstCounter = 0;

  override def draw(canvas: Canvas){
    val d = if(isBurst){
      burstDrawable 
    } else {
      drawable
    }
    val cw = canvas.getWidth()
    val ch = canvas.getHeight()
    val x = (xPercent * cw).toInt;
    val y = (yPer * ch).toInt;
    d.setBounds(x-sz/2, y-sz/2, x+sz/2, y+sz/2);
    d.draw(canvas);
  }
  def move(){
    sz = Math.min(sz+2, size)
    yPer = yPer - speed;
    if(isBurst){
      burstCounter = burstCounter + 1
    } else {
      yPer = yPer - speed;
    }
  }
  def burst(){
    isBurst = true
  }
  def valid(): Boolean = {
    return yPercent > -0.1 && burstCounter < 5
  }

}
