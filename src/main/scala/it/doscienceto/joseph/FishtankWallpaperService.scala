package it.doscienceto.joseph

import android.app.Activity
import android.os.Handler;
import android.view.{SurfaceHolder, MotionEvent};
import android.service.wallpaper.WallpaperService
import android.graphics.{Canvas, BitmapFactory};
import java.util.Random;

class FishtankWallpaperService extends WallpaperService {

  class FishtankWallpaperEngine extends this.Engine {
    val handler = new Handler();
    val background = new Background(getResources().getDrawable(R.drawable.background));
    val rng = new Random();
    val fishDrawables = List(
      R.drawable.fish_01,
      R.drawable.fish_02, 
      R.drawable.fish_03,
      R.drawable.fish_04,
      R.drawable.fish_05,
      R.drawable.fish_06,
      R.drawable.fish_07
    ).map(BitmapFactory.decodeResource(getResources(), _));

    val fishCount = rng.nextInt(3)+5;

    val bubbleDrawables = List(
      R.drawable.bubble_01,
      R.drawable.bubble_02, 
      R.drawable.bubble_03,
      R.drawable.bubble_04,
      R.drawable.bubble_05
    ).map(getResources().getDrawable(_));

    val bubbleBurstDrawable = getResources().getDrawable(R.drawable.bubble_burst)
    val weedDrawables = List(
      (R.drawable.weed_01_01,
       R.drawable.weed_01_02), 
      (R.drawable.weed_02_01,
       R.drawable.weed_02_02),
      (R.drawable.weed_03_01,
       R.drawable.weed_03_02)
    ).map {case (a, b) => (getResources().getDrawable(a), getResources().getDrawable(b))};

    def getWeed():Seq[Weed] = {
      val weedCount = rng.nextInt(16) + 4;
      val zInds = Range(0, weedCount).map(x=>rng.nextInt()).sortBy(x=>x);
      val yPers = Range(0, weedCount).map(x=>rng.nextFloat() * 0.15f + 0.85f).sortBy(x=>x);
      (zInds zip yPers).map {case (zInd, yPer) => {
          val theType = rng.nextInt(weedDrawables.length);
          new Weed( 
            weedDrawables(theType)._1,
            weedDrawables(theType)._2,
            theType * 225 + 150 + rng.nextInt(225),
            rng.nextFloat(),
            yPer,
            rng.nextBoolean(),
            zInd
          )
        }
      }
    }
    val weed: Seq[Weed] = if(rng.nextDouble > 0.25){
      getWeed()
    } else {
      List.empty
    }

    private def randomFish(): Fish = {
      new Fish(
        fishDrawables(rng.nextInt(fishDrawables.length)),
        rng.nextInt(360) + 240,
        rng.nextFloat() * 0.75f,
        rng.nextInt(2)*2 - 1, 
        rng.nextFloat() *0.005f + 0.002f,
        rng.nextInt()
      )
    }

    var fish = Range(0, fishCount).map( _ => randomFish())
    var bubbles :Array[Bubble]= Array[Bubble]();

    def addBubble(height:Int){
        val theFish = fish(rng.nextInt(fish.length));
          val x = theFish.xpercent;
          val y = theFish.ypercent+ 0.5f*theFish.height/height;
          addBubbleAtPoint(x, y, theFish.zIndex +1);
    }
    def addBubbleAtPoint(x:Float, y:Float, zIndex:Int){
        val bubble = new Bubble(
          bubbleDrawables(rng.nextInt(bubbleDrawables.length)),
          bubbleBurstDrawable,
          x,
          y,
          0.0025f, 
          rng.nextInt(30) + 36,
          zIndex);
        bubbles = bubbles :+ bubble;
    }

 
    
    class Schedule extends Runnable {
      override def run() :Unit = {
        draw();
      }
    };
    val runner = new Schedule()
    handler.post(runner);

    private def draw(): Unit = {
      val holder = getSurfaceHolder();
      val canvas = holder.lockCanvas();
      if (canvas != null) {
        background.draw(canvas);
        (fish++bubbles++weed).sortBy(_.zIndex).foreach(_.draw(canvas));
        fish = fish.filter(_.onScreen(canvas.getWidth));
        bubbles = bubbles.filter(_.valid);


        if(fish.length < fishCount){
          val newFish = fishCount - fish.length;
          fish = fish ++ Range(0, newFish).map( _ => randomFish())
        }
  
        if(rng.nextDouble < 0.025){
          addBubble(canvas.getHeight())
        }

        holder.unlockCanvasAndPost(canvas);
      }
      bubbles.foreach(_.move());
      bubbles.foreach(x=>{
        if(rng.nextDouble < 0.001){
          x.burst()
        }
      })
      fish.foreach(_.move());
      weed.foreach(x=>{
        if(rng.nextDouble < 0.005){
          x.toggle()
        }
      })
      handler.postDelayed(runner, 1000/90);
    }

    override def onCreate(holder:SurfaceHolder): Unit = {
      draw()
    }

    override def onVisibilityChanged(visible: Boolean):Unit = {
      if (visible) {
        handler.post(runner);
      } else {
        handler.removeCallbacks(runner);
      }
    }
    def getBounds(): Tuple2[Int, Int] = {
      val holder = getSurfaceHolder();
      val frame = holder.getSurfaceFrame();
      (frame.right, frame.bottom)
    }
    override def onTouchEvent(event: MotionEvent):Unit = {
      val (width, height) = getBounds()
      val x = event.getX().toFloat/width;
      val y = event.getY().toFloat/height;
      addBubbleAtPoint(x, y, rng.nextInt());
    }
  }

  override def onCreateEngine() :Engine = {
    new FishtankWallpaperEngine()
  }


}
